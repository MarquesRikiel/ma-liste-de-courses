package com.example.riiki.myapplication


import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.MenuItem
import com.backendless.Backendless
import com.backendless.persistence.DataQueryBuilder
import kotlinx.android.synthetic.main.activity_page.*
import kotlinx.android.synthetic.main.activity_subscribe.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.alert
import org.jetbrains.anko.coroutines.experimental.bg
import org.jetbrains.anko.db.asMapSequence
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.select
import org.jetbrains.anko.startActivityForResult
import org.jetbrains.anko.toast
import android.support.v7.widget.helper.ItemTouchHelper
import android.support.v7.widget.RecyclerView
import android.support.design.widget.Snackbar
import android.widget.Toast
import android.app.Activity
import android.app.Activity.RESULT_OK
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.PorterDuff
import android.support.v4.content.ContextCompat
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.view.View
import com.example.riiki.myapplication.R.id.btn_send
import kotlinx.android.synthetic.main.activity_recherche.*
import kotlinx.android.synthetic.main.fragment_liste.*


class PageActivity : AppCompatActivity(), DetailFragment.Listener, ListeFragment.Listener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_page)
        fragmentManager.beginTransaction().replace(R.id.frag_liste_ou_map, ListeFragment()).commit()


        btn_send.setOnClickListener {
            // Il faut qu'on soit pr�venu quand l'activit� Ajouter est finie pour pouvoir mettre � jour la liste du RecyclerView
           startActivityForResult<AjouterActivity>(1)
        }

    }


    lateinit var menuMap: MenuItem


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        val searchView = menu.findItem(R.id.action_search).actionView as SearchView
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))

        menuMap = menu.findItem(R.id.action_map)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.delete ->  {
                // On passe le message au fragement (cas tablette)
                val fragDet = fragmentManager.findFragmentById(R.id.frag_detail) as DetailFragment
                fragDet.supprimerItemActif()
                return true
            }
            R.id.action_map -> {
                // On affiche la boite de dialogue

                alert("Voulez-vous supprimer tous les articles ?") {
                    title = "Vider la liste"
                    positiveButton("Oui") {


                        val listeMessage = arrayListOf<MessageRVD>()
                        var total: Long = 0

                        // On select sur la base de donn�es
                        dbProduits.use {
                            select(DBProduits.TABLE_PRODUITS
                            ).exec {
                                // Tous les messages
                                for (row in asMapSequence()) {
                                    delete(DBProduits.TABLE_PRODUITS)
                                }
                            }
                        }
                        recreate()



                            if(MainActivity.sharedPref.getBoolean(MainActivity.PREF_INVITE,true)) {
                                async(UI) {
                                val count = bg {
                                    Backendless.Persistence
                                            .of(Article::class.java)
                                            .getObjectCount()
                                }.await()
                                val articles = bg {
                                    Backendless.Persistence.of(Article::class.java)
                                            .find(DataQueryBuilder.create()
                                                    .setPageSize(count).setOffset(0))
                                }.await()
                                    var i = 0
                                while (i<count) {
                                    bg {
                                        Backendless.Persistence
                                                .of(Article::class.java).remove(articles[i])
                                    }.await()
                                    i+=1
                                }
                            }
                        }

                        toast("Liste vidé") }
                    negativeButton("Non") { }
                }.show()



                // Le fragment (on est obligatoirement sur tablette si cette fonction est appell�e) nous informe que le message actuellement affich� est supprim�
                // On RE-charge la liste des message depuis la base de donn�es
                //val fragListe = fragmentManager.findFragmentById(R.id.frag_liste) as ListeFragment
                //fragListe.chargerListe()

                // En attendant, on affiche "message introvable" dans le fragment en passant un id inexistant
                //val fragDet = fragmentManager.findFragmentById(R.id.frag_detail) as DetailFragment
                //fragDet.afficherDetail(-1)


                return true
            }

            R.id.Deconnexion ->  {
                val intent = Intent(applicationContext, MainActivity::class.java)
                //intent.putExtra(MainActivity.PREF_LOGIN, true)
                MainActivity.sharedPref.edit().putBoolean(MainActivity.PREF_LOGIN,false).apply();
                MainActivity.sharedPref.edit().putBoolean(MainActivity.PREF_INVITE,false).apply();
                startActivity(intent)
                finish()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            1 -> {
                // AjouterActivity, sur t�l�phone et tablette
                if (resultCode == RESULT_OK) {
                    // Est-ce qu'un message a r�ellement �t� ajouter ou est-ce que l'utilisateur a annul� ?
                    val r = data?.getBooleanExtra(AjouterActivity.EXTRA_LIST_CHANGED, false) ?: false
                    if (r) {
                        // On RE-charge la liste des message depuis la base de donn�es
                        val fragListe = fragmentManager.findFragmentById(R.id.frag_liste_ou_map) as ListeFragment
                        fragListe.chargerListe()
                    }
                }
            }
            2 -> {
                // DetailActivity: sur t�l�phone uniquement (sur tablette, c'est onMessageDelete qui fait �a)
                if (resultCode == RESULT_OK) {

                    /**********************
                    val fragListe = fragmentManager.findFragmentById(R.id.frag_liste) as ListeFragment
                    fragListe.chargerListe()
                    **********************/

                    // Est-ce que le message affich� a �t� supprim� ou est-ce que l'utilisateur est juste revenu � la liste sans le modifier ?
                    val r = data?.getBooleanExtra(DetailActivity.EXTRA_LIST_CHANGED, false) ?: false
                    if (r) {
                        // On RE-charge la liste des message depuis la base de donn�es
                        val fragListe = fragmentManager.findFragmentById(R.id.frag_liste_ou_map) as ListeFragment
                        fragListe.chargerListe()
                    }
                }
            }
        }
    }

    override fun onMessageSelection(id: Long) {
        // Notre fragment de liste nous indique que l'utilisateur a cliqu� sur un message

        // On regarde si le fargment Detail existe
        val fragDet = fragmentManager.findFragmentById(R.id.frag_detail) as DetailFragment?
        if(fragDet != null){
            // On est en layour sw600dp, c'est � dire qu'on est sur un tablette
            // On dit au fragment d'afficer le message avec l'id cliqu� dans la liste
            fragDet.afficherDetail(id)
        }else{
            // Pas de fragment, on est sur le layout sans modifieur, donc sur t�l�phone
            // On lance une nouvelle activit� Detail avec en param�tre l'id cliqu� dans la liste
            startActivityForResult<DetailActivity>(2, DetailActivity.EXTRA_MESSAGE_ID to id)
        }
    }

    override fun onMessageDelete() {
        // Le fragment (on est obligatoirement sur tablette si cette fonction est appell�e) nous informe que le message actuellement affich� est supprim�
        // On RE-charge la liste des message depuis la base de donn�es
        val fragListe = fragmentManager.findFragmentById(R.id.frag_liste) as ListeFragment
        fragListe.chargerListe()

        // En attendant, on affiche "message introvable" dans le fragment en passant un id inexistant
        val fragDet = fragmentManager.findFragmentById(R.id.frag_detail) as DetailFragment
        fragDet.afficherDetail(-1)
    }


}


