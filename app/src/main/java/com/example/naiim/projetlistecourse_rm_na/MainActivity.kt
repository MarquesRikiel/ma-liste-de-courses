﻿package com.example.riiki.myapplication


import android.support.v7.app.AppCompatActivity
import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import android.widget.Toast
import com.backendless.Backendless
import com.backendless.persistence.DataQueryBuilder
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg
import org.jetbrains.anko.startActivityForResult
import android.content.SharedPreferences
import kotlinx.android.synthetic.main.activity_ajouter.*
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.toast


class MainActivity : AppCompatActivity() {
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    companion object {
        const val PREF_LOGIN = "login"
        const val PREF_INVITE = "invite"
        lateinit var sharedPref: SharedPreferences
    }
    //public lateinit var sharedPref: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Backendless.initApp(this, "49B79320-038F-1AE4-FF8E-DBC5DF66B800","87747486-C02F-5551-FFE1-1466FE293500")
        setContentView(R.layout.activity_main)
        // Set up the login form.
        sharedPref = getSharedPreferences(PREF_LOGIN, MODE_PRIVATE);
        sharedPref = getSharedPreferences(PREF_INVITE, MODE_PRIVATE);

        if(sharedPref.getBoolean(PREF_LOGIN,false) || sharedPref.getBoolean(PREF_INVITE,false)){
            goToPageActivity();
        }

        btn_subscribe.setOnClickListener{
            val intent = Intent(this, SubscribeActivity::class.java)
            startActivity(intent)
        }

        email_sign_in_button.setOnClickListener {
            if (email.text.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email.text.toString()).matches()){
                email?.error = "Veuillez entrer un email valide"
            }
            else if (usr_password.text.isEmpty()){
                usr_password?.error = "Veuillez entrer votre mot de passe"
            }
            else {
                async(UI) {
                    Toast.makeText(this@MainActivity, "Connexion en cours...", Toast.LENGTH_SHORT).show()
                    val res = bg {
                        Backendless.Persistence.of(User::class.java)
                                .find(DataQueryBuilder.create().setPageSize(1).setOffset(0).setWhereClause("email = '${email.text}' " +
                                        "and password = '${usr_password.text}'"))
                    }.await()

                    if (res.count() > 0) {

                            val count = bg {
                                Backendless.Persistence
                                        .of(Article::class.java)
                                        .getObjectCount()
                            }.await()
                            val articles = bg {
                                Backendless.Persistence.of(Article::class.java)
                                        .find(DataQueryBuilder.create()
                                                .setPageSize(count).setOffset(0))
                            }.await()
                            for (artlc in articles) {


                                dbProduits.use {
                                    insert(DBProduits.TABLE_PRODUITS,
                                            DBProduits.COLUMN_PRODUITS_QUANTITE to artlc.quantity.toString(),
                                            DBProduits.COLUMN_PRODUITS_ARTICLE to artlc.article.toString(),
                                            DBProduits.COLUMN_PRODUITS_PRIX to artlc.price.toString())
                                }
                            }

                        goToPageActivity()
                        sharedPref.edit().putBoolean(PREF_LOGIN,true).apply();

                    } else {
                        Toast.makeText(this@MainActivity, "Login ou Mot de passe incorrect", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }

        btn_invite.setOnClickListener{
            goToPageActivity()
            sharedPref.edit().putBoolean(PREF_INVITE,true).apply();
        }
    }


    fun goToPageActivity() {
        val intent = Intent(applicationContext, PageActivity::class.java)
        // intent.putExtra(PageActivity.LOGIN, email.text.toString())
        startActivity(intent)
        finish()
    }

}