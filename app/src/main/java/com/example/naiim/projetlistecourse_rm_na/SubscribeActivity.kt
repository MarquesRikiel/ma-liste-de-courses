package com.example.riiki.myapplication

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.util.Patterns

import android.widget.Toast
import com.backendless.Backendless
import kotlinx.android.synthetic.main.activity_subscribe.*

import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg
import java.util.*

data class User(var email: String ="",
                var password: String ="")

class SubscribeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subscribe)
        Backendless.initApp(this, "49B79320-038F-1AE4-FF8E-DBC5DF66B800", "87747486-C02F-5551-FFE1-1466FE293500")

        btn_send_subscribe.setOnClickListener {
            //Toast.makeText(this, "mdp = '${usr_pwd.text}' mdp_conf = '${usr_pwd_conf.text}'", Toast.LENGTH_SHORT).show()
            if (usr_email.text.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(usr_email.text.toString()).matches()){
                usr_email?.error = "Veuillez entrer un email valide"
            }
            else if (usr_email.text.toString() != usr_conf_email.text.toString()) {
                usr_conf_email?.error = "Adresses e-mail differentes"
            }
            else if (usr_password.text.isEmpty() || usr_password.text.length<6){
                usr_password?.error = "Veuillez entrer un mot de passe de min. 6 caratères"
            }
            else if (usr_password.text.toString() != usr_conf_password.text.toString()) {
                usr_conf_password?.error = "Mots de passe differents"
            }


            else {

            val user = User(usr_email.text.toString(), usr_password.text.toString())
            async(UI) {
                bg {
                    Backendless.Persistence.of(User::class.java).save(user)

                }.await()
                Toast.makeText(this@SubscribeActivity, "Enregistré", Toast.LENGTH_SHORT).show()
                intent = Intent(this@SubscribeActivity, PageActivity::class.java)
                MainActivity.sharedPref.edit().putBoolean(MainActivity.PREF_LOGIN,true).apply();
                startActivity(intent)
                finish()
            }
        }
             }
        }
    }

