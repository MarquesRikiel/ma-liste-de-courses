package com.example.riiki.myapplication

import android.app.Fragment
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.ContactsContract
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.backendless.Backendless
import com.backendless.persistence.DataQueryBuilder
import com.example.riiki.myapplication.MainActivity.Companion.PREF_INVITE
import com.example.riiki.myapplication.MainActivity.Companion.sharedPref
import com.google.android.gms.maps.CameraUpdate
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.MapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.fragment_detail.*
import kotlinx.coroutines.experimental.android.UI
import org.jetbrains.anko.coroutines.experimental.bg
import org.jetbrains.anko.db.asMapSequence
import org.jetbrains.anko.db.select
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.sendSMS
import org.jetbrains.anko.toast
import java.util.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async


class DetailFragment : Fragment(){
    interface Listener {
        // Quand on supprime un message, il faut prévenir MainActivity de mettre à jour la liste
        fun onMessageDelete()
    }

    var mListener: Listener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            mListener = context as Listener
        } catch (e: ClassCastException) {
            throw ClassCastException(context.toString() + " must implement DetailFragment.Listener")
        }
    }

    // L'id du message actuellement affiché
    var idMessage : Long = -1
    var article : String = ""
    var quantity : Long = 0
    var price : String = ""


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // Il faut utiliser childFragmentManager au lieu de fragmentManager quand on est nous même dans un fragment
        /*val carte = childFragmentManager.findFragmentById(R.id.carte) as MapFragment
        // On désactive tout, carte statique
        carte.getMapAsync {
            it.uiSettings.isZoomControlsEnabled = false
            it.uiSettings.isMyLocationButtonEnabled = false
            it.uiSettings.setAllGesturesEnabled(false)
        }
        */

        fab_share.setOnClickListener {
            // On lance la sélection d'un contact (mais pas encore le partage, ça ce sera dans onActivityResult quand on aura les données du contacts)
            val intentContact = Intent(Intent.ACTION_PICK)
            intentContact.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE

            // le "this" implicite devant "packageManager" devient "activity." quand on est dans un fragment
            if (intentContact.resolveActivity(activity.packageManager) != null) {
                startActivityForResult(intentContact, 10)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_detail, container, false)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            10 -> {
                // Contact
                if (resultCode == AppCompatActivity.RESULT_OK) {
                    // le "this" implicite devant "contentResolver" devient "activity." quand on est dans un fragment
                    with(activity.contentResolver.query(data?.data, arrayOf(ContactsContract.CommonDataKinds.Phone.NUMBER), null, null, null)) {
                        moveToFirst()
                        val telephone = getString(0)
                        // Pareil pour "sensSMS"
                        activity.sendSMS(telephone, "A acheter : " + tv_expediteur.text.toString() + " x " + tv_message.text.toString())
                        // On envoie le contenu de tv_message, qui lui-même contient DBMessages.COLUMN_MESSAGES_CONTENU
                    }
                }
            }
        }
    }


    fun supprimerItemActif(){
        // L'ancien code du botuon delete est maintenant une fonction pour pouvoir être appelé soit par DetailActivity (cas téléphone) soit par MaintActivity (cas tablette) quand on appuie sur le bouton de action bar corbeille
        async(UI) {
            // On supprime l'article actuellement affiché de la base de données
            activity.dbProduits.use {
                delete(DBProduits.TABLE_PRODUITS,
                        "${DBProduits.COLUMN_PRODUITS_ID} = {id}",
                        "id" to idMessage)
            }

            if(sharedPref.getBoolean(PREF_INVITE,true)) {

                val count = bg {
                    Backendless.Persistence
                            .of(Article::class.java)
                            .getObjectCount()
                }.await()
                val articles = bg {
                    Backendless.Persistence.of(Article::class.java)
                            .find(DataQueryBuilder.create()
                                    .setPageSize(count).setOffset(0)
                                    .setWhereClause("article = '${article}'"))
                }.await()
                for (artcl in articles) {
                    toast("Articles = ${artcl.article}")
                }
                bg {
                    Backendless.Persistence
                            .of(Article::class.java).remove(articles[0])
                }
                // On prévient notre activité :
                // si on est sur tablette, ce fragment est directement dans PageActivity qui va mettre à la jour le RecyclerView
                // si on est sur téléphone, ce fragment est dans DetailActivity, c'est lui qui va prévenir PageActivity pour nous
                mListener!!.onMessageDelete()
            }
        }
    }

    fun afficherDetail(id: Long) {
        // On sauvegarde l'id affiché
        idMessage = id

        // On charge l'article de la base de données
        activity.dbProduits.use {
            select(DBProduits.TABLE_PRODUITS, // Table
                    DBProduits.COLUMN_PRODUITS_ID, // Toutes les colones
                    DBProduits.COLUMN_PRODUITS_ARTICLE,
                    DBProduits.COLUMN_PRODUITS_PRIX,
                    DBProduits.COLUMN_PRODUITS_QUANTITE)
                    .whereArgs("${DBProduits.COLUMN_PRODUITS_ID} = {id}", // On va charger un message à la fois avec comme identifiant id
                            "id" to idMessage)
                    .exec {
                        if(count == 0){
                            // Au cas où il n'y est aucun message
                            val am = activity.getString(R.string.aucun_produit)
                            tv_pk.text = am
                            tv_expediteur.text = am
                            tv_date.text = am
                            tv_message.text = am
                        }else {
                            // 1 message maximum retourné, parce que les id sont uniques (PK)
                            for (row in asMapSequence()) {
                                // On ne passe qu'une seule fois dans le for du coup

                                article = row[DBProduits.COLUMN_PRODUITS_ARTICLE] as String
                                quantity = row[DBProduits.COLUMN_PRODUITS_QUANTITE] as Long
                                price = row[DBProduits.COLUMN_PRODUITS_PRIX] as String
                                // Affichage dans le XML
                                tv_pk.text = (row[DBProduits.COLUMN_PRODUITS_ID] as Long).toString()
                                tv_expediteur.text = row[DBProduits.COLUMN_PRODUITS_ARTICLE] as String
                                //val date = Date()
                                //date.time = row[DBProduits.COLUMN_PRODUITS_PRIX] as Long
                                tv_date.text = row[DBProduits.COLUMN_PRODUITS_PRIX] as String
                                tv_message.text = (row[DBProduits.COLUMN_PRODUITS_QUANTITE] as Long).toString()


                            }
                        }
                    }
        }
    }
}

