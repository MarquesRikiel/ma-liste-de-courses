package com.example.riiki.myapplication

import android.app.SearchManager
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import org.jetbrains.anko.toast

class RechercheActivity : AppCompatActivity(), ListeFragment.Listener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recherche)
    }

    override fun onStart() {
        super.onStart()

        if (intent.action == Intent.ACTION_SEARCH) {
            val query = intent.getStringExtra(SearchManager.QUERY)
            toast("Resultats pour " + query)
            // Ma liste de résultats de recherche réutilise le même feagment Liste que MainActivity
            val fragListe = fragmentManager.findFragmentById(R.id.frag_liste) as ListeFragment

            // J'ai ajouté dans ce fragment une fonction qui affiche seulement les messages correspondant une String (au lieu de tous)
            fragListe.chargerListeMotClef(query)
        }

        // Je l'ai mis dans onStart parce que fragListe.onCreated apelle chargerListe APRES la fin du onCreate de ici (du coup si j'apelle chargerListeMotClef dans onCreate elle est effacée)
    }

    override fun onMessageSelection(id: Long) {
        // Quand l'utilisateur choisit un élement dans la liste des résultats, on retourne à l'écran précédant (MainActivity a priori)
        // On pourrait aussi ouvrir cet élément, mais alors il faut gérer le cas téléphone (affichange du détail dans une activité séparée) et le cas tablette (list et détail sur MainActivity)
        finish()
    }
}
