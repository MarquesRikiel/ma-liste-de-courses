package com.example.riiki.myapplication




import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*

val Context.dbProduits: DBProduits
    get() = DBProduits.getInstance(applicationContext)

class DBProduits(ctx: Context) : ManagedSQLiteOpenHelper(ctx, DATABASE_NAME, null, DATABASE_VERSION) {
    companion object {
        const val DATABASE_NAME = "produits.db"
        const val DATABASE_VERSION = 12

        const val TABLE_PRODUITS = "produits"
        const val COLUMN_PRODUITS_ID = "id"
        const val COLUMN_PRODUITS_ARTICLE = "article"
        const val COLUMN_PRODUITS_QUANTITE = "quantite"
        const val COLUMN_PRODUITS_PRIX = "prix"

        private var instance: DBProduits? = null

        @Synchronized
        fun getInstance(ctx: Context): DBProduits {
            if (instance == null) {
                instance = DBProduits(ctx)
            }
            return instance!!
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.createTable(TABLE_PRODUITS, true,
                COLUMN_PRODUITS_ID to INTEGER + PRIMARY_KEY,
                COLUMN_PRODUITS_ARTICLE to TEXT + NOT_NULL,
                COLUMN_PRODUITS_QUANTITE to INTEGER + NOT_NULL,
                COLUMN_PRODUITS_PRIX to TEXT + NOT_NULL
        )
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        if(oldVersion < 11) {
            // Mise à jour de la v10 à la v11
        }

    }
}
