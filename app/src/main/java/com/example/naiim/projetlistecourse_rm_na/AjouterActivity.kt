package com.example.riiki.myapplication

import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.location.Geocoder
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.view.View
import com.backendless.Backendless
import com.example.riiki.myapplication.MainActivity.Companion.PREF_INVITE
import com.example.riiki.myapplication.MainActivity.Companion.PREF_LOGIN
import com.example.riiki.myapplication.MainActivity.Companion.sharedPref
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import kotlinx.android.synthetic.main.activity_ajouter.*
import org.jetbrains.anko.coroutines.experimental.bg
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.imageBitmap
import org.jetbrains.anko.longToast
import org.jetbrains.anko.startActivityForResult
import org.jetbrains.anko.toast
import java.io.File
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*



data class Article(var objectId: String? = null,
                   var article: String = "",
                   var price: String = "",
                   var quantity: Int = 0)


class AjouterActivity : AppCompatActivity() {
    companion object {
        const val EXTRA_LIST_CHANGED = "AjouterActivity.EXTRA_LIST_CHANGED"

    }

    lateinit var price : String
    lateinit var quantity : String




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ajouter)
        // Backendless.initApp(this, "49B79320-038F-1AE4-FF8E-DBC5DF66B800","87747486-C02F-5551-FFE1-1466FE293500")



        btn_send.setOnClickListener {

            if(txt_prix.text.toString() == null || txt_prix.text.toString() == ""){
                price = "0"
            }
            else{
                price = txt_prix.text.toString()
            }
            if(txt_message.text.toString() == null || txt_message.text.toString() == ""){
                quantity = "0"
            }
            else{
                quantity = txt_message.text.toString()
            }
            if(txt_expediteur.text.toString() == null || txt_expediteur.text.toString() == ""){
                txt_expediteur?.error = "Veuillez entrer un produit"
            }else{
                envoyer()
            }

        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
    }



    fun envoyer(){

        dbProduits.use {
            insert(DBProduits.TABLE_PRODUITS,
                    DBProduits.COLUMN_PRODUITS_QUANTITE to quantity.toString(),
                    DBProduits.COLUMN_PRODUITS_ARTICLE to txt_expediteur.text.toString(),

                    DBProduits.COLUMN_PRODUITS_PRIX to price.toString())
        }


        if(sharedPref.getBoolean(PREF_INVITE,true)) {
            val atcl = Article(null, txt_expediteur.text.toString(), price, quantity.toInt())
            bg {
                Backendless.Persistence .of(Article::class.java).save(atcl)
            }
        }
            toast(R.string.produit_ajoute)
            // On prévient MainActivity que la liste a changé
            val retIntent = Intent()
            retIntent.putExtra(EXTRA_LIST_CHANGED, true)
            setResult(Activity.RESULT_OK, retIntent)
            // Et on ferme l'écran de saisie et on revient à MainActivity
            // Ca va aussi effacer tous les champs
            finish()


    }
}
