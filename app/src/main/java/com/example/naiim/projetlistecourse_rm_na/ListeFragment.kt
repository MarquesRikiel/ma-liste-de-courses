package com.example.riiki.myapplication


import android.app.Activity
import android.app.Fragment
import android.content.Context
import android.graphics.*
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_liste.*
import org.jetbrains.anko.db.asMapSequence
import org.jetbrains.anko.db.select
import java.util.*
import android.graphics.ColorSpace.Model
import kotlinx.coroutines.experimental.async
import android.support.v7.widget.RecyclerView
import android.widget.Adapter
import android.support.v7.widget.helper.ItemTouchHelper
import android.support.design.widget.Snackbar
import com.backendless.Backendless
import com.backendless.persistence.DataQueryBuilder
import kotlinx.coroutines.experimental.android.UI
import org.jetbrains.anko.coroutines.experimental.bg
import org.jetbrains.anko.toast


class ListeFragment : Fragment(){
    interface Listener {
        // Quand l'utilisateur clique sur un message, on prévient l'activity
        fun onMessageSelection(id: Long)
    }

    var mListener: Listener? = null


    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            mListener = context as Listener
        } catch (e: ClassCastException) {
            throw ClassCastException(context.toString() + " must implement ListeFragment.Listener")
        }
    }

    // Pour résoudre un bug avec les anciennes versions d'android
    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        try {
            mListener = activity as Listener
        } catch (e: ClassCastException) {
            throw ClassCastException(activity.toString() + " must implement ListeFragment.Listener")
        }
    }



    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        chargerListe()


    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_liste, container, false)
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
    }



    fun chargerListe() {

        var total = 0.0


        // Les messages qui sont affichés dans le RecyclerView
        val listeMessage = arrayListOf<MessageRVD>()

        //if(MainActivity.sharedPref.getBoolean(MainActivity.PREF_INVITE,false)) {
            // On select sur la base de données
            activity.dbProduits.use {
                select(DBProduits.TABLE_PRODUITS, // Table
                        DBProduits.COLUMN_PRODUITS_ID, // Les colones de MessageRVD uniquement
                        DBProduits.COLUMN_PRODUITS_ARTICLE,
                        DBProduits.COLUMN_PRODUITS_QUANTITE,
                        DBProduits.COLUMN_PRODUITS_PRIX
                ).exec {
                    // Tous les messages
                    for (row in asMapSequence()) {


                        listeMessage.add(MessageRVD(
                                row[DBProduits.COLUMN_PRODUITS_ID] as Long,
                                row[DBProduits.COLUMN_PRODUITS_ARTICLE] as String,
                                row[DBProduits.COLUMN_PRODUITS_QUANTITE] as Long
                        ))
                        total += ((row[DBProduits.COLUMN_PRODUITS_PRIX] as String)).toFloat() * (row[DBProduits.COLUMN_PRODUITS_QUANTITE] as Long)

                    }
                }
            }
        rv_liste.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        rv_liste.adapter = MessageRecyclerAdapter(listeMessage){
            // On prévient MainActivity que l'utilisateur a cliqué sur un message
            mListener!!.onMessageSelection(it.id)
        }
        //tv_tot.setText(total.toInt())
        tv_tot.text = ("%.2f".format(total)).toString()
        /*}
        else{



            async(UI) {
                val count = bg {
                    Backendless.Persistence
                            .of(Article::class.java)
                            .getObjectCount()
                }.await()
                toast("Il y a $count articles")

                val articles = bg {
                    Backendless.Persistence.of(Article::class.java)
                            .find(DataQueryBuilder.create()
                                    .setPageSize(count).setOffset(0))
                }.await()
                var i: Int = 1
                for (artlc in articles) {
                    toast("Articles = ${articles.count()}")
                    toast(""+ artlc.objectId)
                    toast("" + artlc.article)
                    toast("" + artlc.quantity)
                    toast("" + artlc.price)

                    listeMessage.add(MessageRVD(
                            "${i}" as Long,
                            "${artlc.article}" as String,
                            "${artlc.quantity}" as Long
                    ))
                    total += ("${artlc.price}".toFloat() * "${artlc.quantity}" as Long)
                    i = 1 + i
                }
                rv_liste.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
                rv_liste.adapter = MessageRecyclerAdapter(listeMessage){
                    // On prévient MainActivity que l'utilisateur a cliqué sur un message
                    mListener!!.onMessageSelection(it.id)
                }
                //tv_tot.setText(total.toInt())
                // tv_tot.text = ("%.2f".format(total)).toString()
                tv_tot.text = ("%.2f".format(2.4)).toString()
            }
            */
        //}


    }

    fun chargerListeMotClef(q: String) {
        // Les messages qui sont affichés dans le RecyclerView
        val listeMessage = arrayListOf<MessageRVD>()

        // On select sur la base de données
        activity.dbProduits.use {
            select(DBProduits.TABLE_PRODUITS, // Table
                    DBProduits.COLUMN_PRODUITS_ID, // Les colones de MessageRVD uniquement
                    DBProduits.COLUMN_PRODUITS_ARTICLE,
                    DBProduits.COLUMN_PRODUITS_QUANTITE)
                    // Ici on veut un where ! On va dire qu'on fait une recherche par expéditeur
                    .whereArgs("${DBProduits.COLUMN_PRODUITS_ARTICLE} LIKE {q}",
                            "q" to "%$q%").exec {

                for (row in asMapSequence()) {
                    listeMessage.add(MessageRVD(
                            row[DBProduits.COLUMN_PRODUITS_ID] as Long,
                            row[DBProduits.COLUMN_PRODUITS_ARTICLE] as String,
                            row[DBProduits.COLUMN_PRODUITS_QUANTITE] as Long
                    ))
                }
            }
        }

        rv_liste.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        rv_liste.adapter = MessageRecyclerAdapter(listeMessage){
            // On prévient RechercheActivity (pas PageActivity ici) que l'utilisateur a cliqué sur un message
            mListener!!.onMessageSelection(it.id)
        }
    }

}
