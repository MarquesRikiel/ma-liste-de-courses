package com.example.riiki.myapplication


import android.graphics.BitmapFactory
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import org.jetbrains.anko.imageBitmap
import java.util.*
import android.graphics.ColorSpace.Model



// La data class pour contenir les infos à afficher d'un message
data class MessageRVD(val id: Long,
                      val expediteur: String,
                      val date: Long
)

class MessageRecyclerAdapter(val list: MutableList<MessageRVD>, val listener: (MessageRVD) -> Unit) : RecyclerView.Adapter<MessageRecyclerAdapter.MessageViewHolder>() {

    inner class MessageViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var expediteur: TextView = view.findViewById(R.id.tv_expediteur)
        var date: TextView = view.findViewById(R.id.tv_date)
    }


    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        holder.expediteur.text = list[position].expediteur
        holder.date.text = list[position].date.toString()
        holder.itemView.setOnClickListener{ listener(list[position]) }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = MessageViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_message, parent, false))

    override fun getItemCount() = list.size
}